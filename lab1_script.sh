apt-get update
apt-get upgrade
apt-get install nginx
ufw app list
ufw allow 'Nginx HTTP'
ufw status
systemctl status nginx
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
touch /etc/nginx/snippets/self-signed.conf
cd /etc/nginx/snippets/
echo "ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;\n
ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;" > self-signed.conf
cd /etc/nginx/snippets/
wget https://gitlab.com/Lucostus/tinf/-/raw/master/ssl-params.conf
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/default.bak
cd /etc/nginx/sites-available
wget https://gitlab.com/Lucostus/tinf/-/raw/master/default
rm default
mv default.1 default
ufw allow 'Nginx Full'
ufw delete allow 'Nginx HTTP'
nginx -t
systemctl restart nginx
